use ggez::event::{self, EventHandler, KeyCode, KeyMods};
use ggez::graphics::{Color, DrawMode, Mesh, Rect};
use ggez::{conf, Context, ContextBuilder, GameResult};
use rand::prelude::*;

#[derive(Copy, Clone)]
enum Dir {
    Left,
    Right,
    Up,
    Down,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
struct Pos {
    x: i32,
    y: i32,
}

type Vec = Pos;

impl Pos {
    fn new(x: i32, y: i32) -> Self {
        Pos { x, y }
    }
}

#[derive(Copy, Clone)]
struct InputState {
    right: bool,
    left: bool,
    up: bool,
    down: bool,
}

impl InputState {
    fn new() -> Self {
        InputState {
            right: false,
            left: false,
            up: false,
            down: false,
        }
    }
}

struct Snake {
    head: Option<Box<BodyPart>>,
    direction: Dir,
}

impl Snake {
    fn new(ctx: &mut Context, pos: Pos, length: u8, tile_size: Vec) -> Self {
        if length == 0 {
            panic!("Snake length must be larger than 0!");
        }

        Snake {
            head: Some(Box::new(BodyPart::new(
                ctx,
                pos,
                length,
                tile_size,
                Color::new(1.0, 0.0, 0.0, 1.0),
            ))),
            direction: Dir::Right,
        }
    }

    fn update(&mut self) {
        if let Some(head) = &mut self.head {
            head.update_position(self.direction);
        }
    }

    fn get_position(&self) -> Pos {
        if let Some(head) = &self.head {
            head.pos
        } else {
            Pos::new(-1, -1)
        }
    }

    fn check_self_collision(&self) -> bool {
        if let Some(head) = &self.head {
            let mut snake = &head.next;
            while let Some(snake_part) = snake {
                if snake_part.pos == head.pos {
                    return true;
                } else {
                    snake = &snake_part.next;
                }
            }
        }

        false
    }
}

struct BodyPart {
    mesh: Mesh,
    pos: Pos,
    last_pos: Pos,
    size: Vec,
    next: Option<Box<BodyPart>>,
    color: Color,
}

impl BodyPart {
    fn new(ctx: &mut Context, pos: Pos, length: u8, tile_size: Vec, color: Color) -> Self {
        let mut next = None;
        if length > 1 {
            let pos = Pos::new(pos.x - 1, pos.y);
            next = Some(Box::from(BodyPart::new(
                ctx,
                pos,
                length - 1,
                tile_size,
                Color::new(1.0, 0.2, 0.0, 1.0),
            )));
        }

        BodyPart {
            mesh: Mesh::new_rectangle(
                ctx,
                DrawMode::fill(),
                Rect::new(
                    (pos.x * tile_size.x) as f32,
                    (pos.y * tile_size.y) as f32,
                    tile_size.x as f32,
                    tile_size.y as f32,
                ),
                color,
            )
            .unwrap(),
            pos,
            last_pos: pos,
            size: tile_size,
            next,
            color,
        }
    }

    fn update_position(&mut self, dir: Dir) {
        self.last_pos = self.pos;
        if let Some(next) = &mut self.next {
            next.set_position(self.pos);
        }
        match dir {
            Dir::Right => self.pos.x += 1,
            Dir::Left => self.pos.x -= 1,
            Dir::Up => self.pos.y -= 1,
            Dir::Down => self.pos.y += 1,
        }
    }

    fn set_position(&mut self, pos: Pos) {
        self.last_pos = self.pos;
        if let Some(next) = &mut self.next {
            next.set_position(self.pos);
        }
        self.pos = pos;
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        if let Some(next) = &mut self.next {
            next.draw(ctx)?;
        }

        self.mesh = Mesh::new_rectangle(
            ctx,
            DrawMode::fill(),
            Rect::new(
                (self.pos.x * self.size.x) as f32 + 1.0,
                (self.pos.y * self.size.y) as f32 + 1.0,
                self.size.x as f32 - 2.0,
                self.size.y as f32 - 2.0,
            ),
            self.color,
        )
        .unwrap();
        ggez::graphics::draw(ctx, &self.mesh, ggez::graphics::DrawParam::default())
    }
}

struct Food {
    mesh: Mesh,
    pos: Pos,
    size: Vec,
}

impl Food {
    fn new(ctx: &mut Context, pos: Pos, tile_size: Vec) -> Self {
        Food {
            mesh: Self::create_mesh(ctx, pos, tile_size),
            pos,
            size: tile_size,
        }
    }

    fn draw(&self, ctx: &mut Context) -> GameResult<()> {
        ggez::graphics::draw(ctx, &self.mesh, ggez::graphics::DrawParam::default())
    }

    fn get_position(&self) -> Pos {
        self.pos
    }

    fn random_position(&mut self, ctx: &mut Context, min: Vec, max: Vec) -> Pos {
        self.pos = Pos::new(
            rand::thread_rng().gen_range(min.x, max.x),
            rand::thread_rng().gen_range(min.y, max.y),
        );
        self.mesh = Self::create_mesh(ctx, self.pos, self.size);
        self.pos
    }

    fn create_mesh(ctx: &mut Context, pos: Pos, size: Vec) -> Mesh {
        Mesh::new_rectangle(
            ctx,
            DrawMode::fill(),
            Rect::new(
                (pos.x * size.x) as f32 + (size.x as f32 / 4.0),
                (pos.y * size.y) as f32 + (size.y as f32 / 4.0),
                size.x as f32 / 2.0,
                size.y as f32 / 2.0,
            ),
            Color::new(0.0, 0.2, 1.0, 1.0),
        )
        .unwrap()
    }
}

struct Game {
    board_size: Vec, // in number of tiles
    _tile_size: Vec,
    update_rate: u128, // nanoseconds
    timer: u128,       // nanoseconds
    snake: Snake,
    food: Food,
    score: u32,
    input_state: InputState,
    old_input_state: InputState,
    new_dir: Option<Dir>,
}

impl Game {
    fn new(ctx: &mut Context, board_size: Vec, _tile_size: Vec) -> Self {
        let mut food = Food::new(ctx, Pos::new(0, 0), _tile_size);
        food.random_position(ctx, Vec::new(0, 0), board_size);
        Game {
            board_size,
            _tile_size,
            update_rate: 300_000_000,
            timer: 0,
            snake: Snake::new(ctx, Pos::new(2, 2), 3, _tile_size),
            food,
            score: 0,
            input_state: InputState::new(),
            old_input_state: InputState::new(),
            new_dir: None,
        }
    }

    fn update_dir(&mut self) {
        if let Some(new_dir) = self.new_dir {
            match (new_dir, self.snake.direction) {
                (Dir::Right, Dir::Left) => (),
                (Dir::Left, Dir::Right) => (),
                (Dir::Up, Dir::Down) => (),
                (Dir::Down, Dir::Up) => (),
                _ => self.snake.direction = new_dir,
            }
        }
    }

    fn check_wrap_around(&mut self) {
        if let Some(head) = &mut self.snake.head {
            if head.pos.x >= self.board_size.x {
                head.pos.x = 0;
            }
            if head.pos.x < 0 {
                head.pos.x = self.board_size.x - 1;
            }
            if head.pos.y >= self.board_size.y {
                head.pos.y = 0;
            }
            if head.pos.y < 0 {
                head.pos.y = self.board_size.y - 1;
            }
        }
    }

    fn check_collision_food(&self) -> bool {
        if self.snake.get_position() == self.food.get_position() {
            true
        } else {
            false
        }
    }

    fn eat_food(&mut self, ctx: &mut Context) {
        while self.is_occupied_tile(self.food.get_position()) {
            self.food
                .random_position(ctx, Vec::new(0, 0), self.board_size);
        }
        self.score += 10;
        self.lengthen_snake(ctx);
        self.update_rate = (self.update_rate as f64 * 0.98) as u128;
        println!("Score: {}", self.score);
    }

    fn is_occupied_tile(&self, pos: Pos) -> bool {
        let mut snake = &self.snake.head;
        while let Some(snake_part) = snake {
            if snake_part.pos == pos {
                return true;
            } else {
                snake = &snake_part.next;
            }
        }

        false
    }

    fn lengthen_snake(&mut self, ctx: &mut Context) {
        let mut snake_iter = &mut self.snake.head;
        while let Some(snake_part) = snake_iter {
            match snake_part.next {
                None => {
                    snake_part.next = Some(Box::new(BodyPart::new(
                        ctx,
                        snake_part.pos,
                        0,
                        snake_part.size,
                        Color::new(1.0, 0.2, 0.0, 1.0),
                    )));
                    break;
                }
                _ => snake_iter = &mut snake_part.next,
            }
        }
    }

    fn restart(&mut self, ctx: &mut Context) {
        self.update_rate = 300_000_000;
        self.timer = 0;
        self.snake = Snake::new(ctx, Pos::new(2, 2), 3, self._tile_size);
        self.score = 0;
        self.food.random_position(ctx, Vec::new(0,0), self.board_size);

        println!("\n-GAME RESTARTED-\n");
    }
}

impl EventHandler for Game {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        self.timer += ggez::timer::delta(ctx).as_nanos();
        if self.timer >= self.update_rate {
            self.update_dir();
            self.snake.update();
            if self.snake.check_self_collision() {
                self.snake.head = None;
                println!("GAME OVER!");
                println!("Final score: {}", self.score);
            }
            self.check_wrap_around();
            if self.check_collision_food() {
                self.eat_food(ctx)
            }
            self.timer -= self.update_rate;
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        ggez::graphics::clear(ctx, [0.1, 0.1, 0.1, 1.0].into());
        self.food.draw(ctx)?;
        if let Some(head) = &mut self.snake.head {
            head.draw(ctx)?;
        }

        ggez::graphics::present(ctx)?;

        Ok(())
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: KeyCode,
        _keymods: KeyMods,
        _repeat: bool,
    ) {
        match keycode {
            KeyCode::Right => self.input_state.right = true,
            KeyCode::Left => self.input_state.left = true,
            KeyCode::Up => self.input_state.up = true,
            KeyCode::Down => self.input_state.down = true,
            KeyCode::R => self.restart(ctx),
            KeyCode::Escape => ggez::event::quit(ctx),
            _ => (),
        }

        if self.input_state.right == true && self.old_input_state.right == false {
            self.new_dir = Some(Dir::Right);
        }

        if self.input_state.left == true && self.old_input_state.left == false {
            self.new_dir = Some(Dir::Left);
        }

        if self.input_state.up == true && self.old_input_state.up == false {
            self.new_dir = Some(Dir::Up);
        }

        if self.input_state.down == true && self.old_input_state.down == false {
            self.new_dir = Some(Dir::Down);
        }

        self.old_input_state = self.input_state;
    }

    fn key_up_event(&mut self, _ctx: &mut Context, keycode: KeyCode, _keymods: KeyMods) {
        match keycode {
            KeyCode::Right => self.input_state.right = false,
            KeyCode::Left => self.input_state.left = false,
            KeyCode::Up => self.input_state.up = false,
            KeyCode::Down => self.input_state.down = false,
            _ => (),
        }

        self.old_input_state = self.input_state;
    }
}

fn main() {
    let board_size = Vec::new(16, 16);
    let tile_size = Vec::new(64, 64);

    let window_size = (
        (board_size.x * tile_size.x) as f32,
        (board_size.y * tile_size.y) as f32,
    );

    // Make a Context.
    let (ref mut ctx, ref mut event_loop) = &mut ContextBuilder::new("my_game", "Cool Game Author")
        .window_setup(conf::WindowSetup::default().title("SNAKE"))
        .window_mode(conf::WindowMode::default().dimensions(window_size.0, window_size.1))
        .build()
        .expect("aieee, could not create ggez context!");

    // Create an instance of your event handler.
    // Usually, you should provide it with the Context object to
    // use when setting your game up.
    let mut game = Game::new(ctx, board_size, tile_size);

    // Run!
    match event::run(ctx, event_loop, &mut game) {
        Ok(_) => println!("Exited cleanly."),
        Err(e) => println!("Error occured: {}", e),
    }
}
